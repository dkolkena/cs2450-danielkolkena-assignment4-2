// cs2450-PriorityQueue.cpp : Defines the entry point for the console application.
/*
For this project, you will need to create a priority queue which stores items in 
the order they are received, but also bumps higher priority items in front of lower 
priority ones. You won�t need to worry about dequeueing items for this project, just 
make sure they make it to the right spot in the queue.

Write a program that accepts input from the command line and correctly orders the 
input items based on the order they were input and the priority they were assigned. 
The input will be in the format of a name (String) followed by a number (Integer). 
Each string-number pair will be entered one at a time. A higher number indicates a 
higher priority.

You may assume that no item will have the same name, but multiple items may have the 
same priority. You may assume that the input will always be valid, that the names will 
be of reasonable length (<10 characters), and that the numbers will always be integers >0. 
If two items have the same priority, the first one entered should still be the first 
in the list. Once all the items are entered (a blank item signals that input is complete),
print out a list showing the order they are in. For example, the following input:

Sally 2
Joe 3
Bob 2
John 1
Jane 5
Jill 5
(blank)

Produces the following output:

Jane
Jill
Joe
Sally
Bob
John

The first (highest priority) item is at the top, and the last (lowest priority) item is at the bottom. Please note that you must roll your implementation of the priority queue. */


#include "stdafx.h"

using namespace std;

int main()
{




	system("PAUSE");
	return 0;
}

